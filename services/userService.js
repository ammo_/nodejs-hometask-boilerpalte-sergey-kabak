const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    getAll() {
        const items = UserRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    create(data) {
        const item = UserRepository.create(data)
        if(!item) {
            return null;
        }
        return item;
    }

    update(id , data){
        const item = UserRepository.update(id, data)
        if(!item) {
            return null;
        }
        return item;
    }

    delete(data){
        const item = UserRepository.delete(data)
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();