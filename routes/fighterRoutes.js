const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', function (req , res , next) {

    const result = FighterService.getAll()

    if (result){
        res.send(result)
    }
    else {
        res.status(400).send('Some error')
    }

});

router.get('/:id', function (req , res , next) {

    const result = FighterService.search(req.params)

    if (result){
        res.send(result)
    }
    else {
        res.status(404).send('Not Found')
    }

});

router.put('/:id', updateFighterValid , function (req , res , next) {

    const result = FighterService.update(req.params.id, req.body)

    if (result){
        res.send(result)
    }
    else {
        res.status(400).send('Some error')
    }

});

router.delete('/:id', function (req , res , next) {

    const result = FighterService.delete(req.params.id)

    if (result){
        res.send(result)
    }
    else {
        res.status(404).send('Not Found')
    }

});

router.post('/', createFighterValid ,  function (req , res , next) {

    const result = FighterService.create(req.body)

    if (result){
        res.send(result)
    }
    else {
        res.status(400).send('Some error')
    }

});

module.exports = router;