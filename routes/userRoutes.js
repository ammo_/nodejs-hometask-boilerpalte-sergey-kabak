const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', function (req , res , next) {

    const result = UserService.getAll()

    if (result){
        res.send(result)
    }
    else {
        res.status(400).send('Some error')
    }

});

router.get('/:id', function (req , res , next) {

    const result = UserService.search(req.params)

    if (result){
        res.send(result)
    }
    else {
        res.status(404).send('Not Found')
    }

});

router.put('/:id', function (req , res , next) {

    const result = UserService.update(req.params.id, req.body)

    if (result){
        res.send(result)
    }
    else {
        res.status(400).send('Some error')
    }

});

router.delete('/:id', function (req , res , next) {

    const result = UserService.delete(req.params.id)

    if (result){
        res.send(result)
    }
    else {
        res.status(404).send('Not Found')
    }

});

router.post('/', createUserValid ,  function (req , res , next) {

    const result = UserService.create(req.body)

    if (result){
        res.send(result)
    }
    else {
        res.status(400).send('Some error')
    }

});

module.exports = router;