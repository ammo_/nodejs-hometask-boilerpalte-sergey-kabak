const { fighter } = require('../models/fighter');



const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const rules = {
        email : function () {
            if(!req.body.email){
                res.status('400').send('email does not exist')
            }
            if (req.body.email.match(/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/g) == null) {
                res.status('400').send('invalid email')
            }
        },
        phoneNumber : function () {
            if(!req.body.phoneNumber){
                res.status('400').send('Phone Number does not exist')
            }
            if (req.body.phoneNumber.match(/^\+\d{8,10}$/g) == null) {
                res.status('400').send('invalid Phone Number ( correnct format is "+1234567890")')
            }
        },
        power : function () {
            if(!req.body.power){
                res.status('400').send('Power does not exist')
            }
            if (req.body.power.match(/^\d+$/g) == null) {
                res.status('400').send('invalid Power ( Power must comtain only numbers )')
            }
        }
    }

    for(let k in req.body){
        rules[k]();
    }
    
    next()

}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;